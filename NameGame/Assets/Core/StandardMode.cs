﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using WillowTree.NameGame.Core.Models;

public class StandardMode
{

  private int nameCount;
  private int questionsToAsk;
  private ProfileFilter filter;
  private StandardScreen currentScreen;
  private List<Profile> profiles;

  private CompositeDisposable subscriptions = new CompositeDisposable();

  private int totalResults = 0;
  private int correctResults = 0;

  public StandardMode(int nameCount, int questionsToAsk)
  {
    this.nameCount = nameCount;
    this.questionsToAsk = questionsToAsk;
    profiles = Singleton<ProfileManager>.Instance.GetProfiles();
    Singleton<StatsManager>.Instance.StartNewSession();
    GenerateScreen();
  }

  public StandardMode(int nameCount, int questionsToAsk, ProfileFilter filter)
  {
    this.nameCount = nameCount;
    this.questionsToAsk = questionsToAsk;
    this.filter = filter;
    profiles = Singleton<ProfileManager>.Instance.GetFilteredList(filter);
    Singleton<StatsManager>.Instance.StartNewSession();

    //probably shouldnt be too specific with your filter
    if (profiles.Count <= nameCount)
    {
      nameCount = profiles.Count;
    }
    GenerateScreen();
  }

  public void GenerateScreen()
  {
    currentScreen = GameObject.Instantiate(Resources.Load("StandardScreen") as GameObject).GetComponent<StandardScreen>();
    currentScreen.transform.SetParent(GameObject.FindGameObjectWithTag("MainCanvas").transform);

    currentScreen.OnScreenResult.Do(OnScreenResult).Subscribe().AddTo(subscriptions);
    currentScreen.GetComponent<RectTransform>().offsetMin = Vector3.zero;
    currentScreen.GetComponent<RectTransform>().offsetMax = Vector3.one;

    for (int i = 0; i < nameCount; i++)
    {
      Profile p = GetRandomProfile();
      while (currentScreen.ProfileExists(p))
      {
        p = GetRandomProfile();
      }
      currentScreen.AddProfile(p);
    }
    currentScreen.LoadImages();
  }

  public void GoToResults()
  {
    GameObject results = GameObject.Instantiate(Resources.Load("ResultsScreen") as GameObject);
    results.transform.SetParent(GameObject.FindGameObjectWithTag("MainCanvas").transform);
    results.GetComponent<RectTransform>().offsetMin = Vector3.zero;
    results.GetComponent<RectTransform>().offsetMax = Vector3.one;
    results.GetComponent<ResultsScreen>().SetReults(correctResults, totalResults); //shows the screen
  }

  public Profile GetRandomProfile()
  {
    int i = Random.Range(0, profiles.Count);
    return profiles[i];
    // return profiles[Random.Range(0, profiles.Length)];
  }
  public void OnScreenResult(bool correct)
  {
    totalResults++;
    if (correct)
      correctResults++;


    //It'd be btter if we loaded the new screen during the tween
    currentScreen.FadeOut(() =>
    {
      DestroyScreen();
      if (totalResults >= questionsToAsk)
        GoToResults();
      else
        GenerateScreen();
    });
  }

  public void DestroyScreen()
  {
    subscriptions.Dispose();
    subscriptions = new CompositeDisposable();
    GameObject.Destroy(currentScreen.gameObject);
    currentScreen = null;
  }



}
