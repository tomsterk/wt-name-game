using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UniRx;
using UnityEngine;
using WillowTree.NameGame.Core.Models;
using WillowTree.NameGame.Core.Services;

public class ProfileManager
{
  //TODO make private
  private List<Profile> profiles;

  public Subject<List<Profile>> OnProfilesRetrieved = new Subject<List<Profile>>();
  public ProfileManager()
  {
    GetProfilesFromServer();
  }

  public async void GetProfilesFromServer()
  {
    Profile[] profilesArray = await NameGameService.GetProfiles();

    profiles = profilesArray.Where(x => VetProfile(x) == true).ToList();
    OnProfilesRetrieved.OnNext(profiles);
  }

  public List<Profile> GetProfiles()
  {
    return profiles;
  }

  public List<Profile> GetFilteredList(ProfileFilter filter)
  {
    return GetFilteredList(filter.property, filter.expression);
  }

  public List<Profile> GetFilteredList(string property, Regex r)
  {
    return profiles.Where(x => r.IsMatch(x.GetType().GetProperty(property).GetValue(x) as string)).ToList();
  }

  public bool VetProfile(Profile p)
  {
    if (p.Headshot.Url == null)
      return false;

    return true;
  }

}