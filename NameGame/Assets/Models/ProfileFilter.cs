using System.Text.RegularExpressions;

public class ProfileFilter
{
  public Regex expression;
  public string property;

  public ProfileFilter()
  {

  }
  
  public ProfileFilter(Regex expression, string property)
  {
    this.expression = expression;
    this.property = property;
  }
}