using System.Collections.Generic;
using System.Linq;
using WillowTree.NameGame.Core.Models;

public class StatsManager
{
  private List<Session> sessions = new List<Session>();
  public Dictionary<string, ProfileStat> profileStats = new Dictionary<string, ProfileStat>();
  public StatsManager()
  {

  }

  public Session GetCurrentSession()
  {
    return sessions.Last();
  }

  public void AddQuestion(Question q)
  {
    GetCurrentSession().AddQuestion(q);
  }

  public void StartNewSession()
  {
    sessions.Add(new Session());
  }

  public void SetProfiles(List<Profile> profiles)
  {
    profiles.OrderBy(p => p.LastName).ToList().ForEach(p => 
    {
      if (profileStats.ContainsKey(p.FullName) == false)
        profileStats.Add(p.FullName, new ProfileStat(p));
      
    }
    );
  }

  public void AddNewStat(Profile p, bool correct)
  {
    ProfileStat stat;
    if (profileStats.TryGetValue(p.FullName, out stat))
    {
      stat.totalGuesses++;
      if (correct)
        stat.correctGuesses++;
    }


  }
}