using System.Collections.Generic;
using System.Linq;
using WillowTree.NameGame.Core.Models;
public class Question
{
  public bool correct = false;
  public int hintsUsed = 0;

  public List<Profile> profiles = new List<Profile>();
  public string correctName;
  
  public Question(List<Profile> profiles, string correctName, bool correct, int hintsUsed)
  {
    this.profiles = profiles;
    this.correctName = correctName;
    this.correct = correct;
    this.hintsUsed = hintsUsed;
    Singleton<StatsManager>.Instance.AddNewStat(profiles.Where(x => x.FullName == correctName).Single(), correct);
  }


}