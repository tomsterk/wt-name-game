using System.Collections.Generic;
using System.Linq;

public class Session
{
  
  public int correctAnswers
  {
    get
    {
      return questions.Where(q => q.correct == true).Count();
    }
  }

  public int totalAnswers
  {
    get { return questions.Count();}
  }

  public int hintsUsed
  {
    get
    {
      return questions.Sum(q => q.hintsUsed);
    }
  }

  public int questionsWhereHintsUsed
  {
    get
    {
      return questions.Where(q => q.hintsUsed > 0).Count();
    }
  }


  public List<Question> questions = new List<Question>();

  public Session()
  {

  }

  public void AddQuestion(Question q)
  {
    questions.Add(q);
  }
}