using WillowTree.NameGame.Core.Models;

public class ProfileStat
{
  public Profile profile;

  public int correctGuesses = 0;
  public int totalGuesses = 0;

  public ProfileStat(Profile p)
  {
    profile = p;
  }
}