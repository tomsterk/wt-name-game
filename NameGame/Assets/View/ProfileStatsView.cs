﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WillowTree.NameGame.Core.Models;

public class ProfileStatsView : MonoBehaviour
{

  public Image picture;
  public Text fullName;
  public Text results;
  public Text percentage;

  private bool imageSet = false;

  private ProfileStat stats;
	private Profile profile;

	public void UpdateData()
	{
		if (Singleton<StatsManager>.Instance.profileStats.TryGetValue(profile.FullName, out stats))
		{
			SetData(stats);
		}
	}

  public void SetData( ProfileStat stats)
  {
    this.stats = stats;
		profile = stats.profile;

    fullName.text = stats.profile.FullName;
    results.text = string.Format("{0} / {1}", stats.correctGuesses, stats.totalGuesses);
    float num = ((float)stats.correctGuesses / stats.totalGuesses) * 100;
    if (stats.totalGuesses == 0)
      num = 0;
    percentage.text = num.ToString("F0") + "%";

		if (imageSet == false)
			SetImage();
  }

  public async void SetImage()
  {
    Texture2D tex = await Singleton<GetImageService>.Instance.GetImage(stats.profile);
    if (tex != null)
      picture.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);

		imageSet = true;
  }
}
