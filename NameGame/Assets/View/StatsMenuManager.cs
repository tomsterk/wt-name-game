﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsMenuManager : MonoBehaviour
{
  public GameObject profileStatPrefab;
  public GameObject view;
  public Button backButton;

  public GameObject scrollView;

  public static StatsMenuManager Instance;

  private List<ProfileStatsView> views = new List<ProfileStatsView>();
  public void Awake()
  {
    Instance = this;

    Hide();
  }
  public void LoadProfiles()
  {
    foreach (var pair in Singleton<StatsManager>.Instance.profileStats)
    {
      GameObject go = Instantiate(profileStatPrefab);
      go.transform.SetParent(view.transform);
      go.GetComponent<ProfileStatsView>().SetData(pair.Value);
      views.Add(go.GetComponent<ProfileStatsView>());
    }

    backButton.onClick.AddListener(Back);
  }

  public void Show()
  {
		views.ForEach(p => p.UpdateData());

    backButton.gameObject.SetActive(true);
    scrollView.SetActive(true);
  }

	public void Hide()
	{
		 backButton.gameObject.SetActive(false);
    scrollView.SetActive(false);
	}

  public void Back()
  {
    MainMenuManager.Instance.Show();
		Hide();

  }
}
