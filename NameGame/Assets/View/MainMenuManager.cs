﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using WillowTree.NameGame.Core.Models;

public class MainMenuManager : MonoBehaviour
{
  public Button standardButton;
  public Button mattButton;
  public Button statsButton;

  public static MainMenuManager Instance;

  private CompositeDisposable subscriptions = new CompositeDisposable();

  //Unity start
  void Start()
  {
    Instance = this;

    standardButton.interactable = false;
    mattButton.interactable = false;
    statsButton.interactable = false;

    standardButton.onClick.AddListener(StartStandardGame);
    mattButton.onClick.AddListener(StartMattGame);
    statsButton.onClick.AddListener(OpenStatsScreen);


    // Singleton
    Singleton<ProfileManager>.Instance.OnProfilesRetrieved
      .Do(OnProfilesRetrieved)
      .Subscribe()
      .AddTo(subscriptions);

    
  }

  public void Show()
  {
    gameObject.SetActive(true);

  }

  public void OnProfilesRetrieved(List<Profile> profiles)
  {
    standardButton.interactable = true;
    mattButton.interactable = true;
    statsButton.interactable = true;


    Singleton<StatsManager>.Instance.SetProfiles(profiles);
    StatsMenuManager.Instance.LoadProfiles();
  }

  public void StartStandardGame()
  {
    gameObject.SetActive(false);

    StandardMode mode = new StandardMode(4, 10);
  }

  public void StartMattGame()
  {
    gameObject.SetActive(false);
    StandardMode mode = new StandardMode(4, 10, new ProfileFilter(new Regex(@"\b(Matt*(hew)*)\b"), "FullName"));
  }

  public void OpenStatsScreen()
  {
    gameObject.SetActive(false);
    StatsMenuManager.Instance.Show();

  }

}
