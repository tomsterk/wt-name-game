﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using WillowTree.NameGame.Core.Models;
using WillowTree.NameGame.Core.Services;

public class ProfilePicture : MonoBehaviour
{

  public Subject<Profile> OnImageLoaded = new Subject<Profile>();
  public Subject<Profile> OnClicked = new Subject<Profile>();
  string imageUrl = "";
  public Profile profile;
  

  public bool faded = false;
  private Image image;

  private Button button;

  public void Awake()
  {
    image = GetComponent<Image>();
    button = GetComponent<Button>();
    button.onClick.AddListener(OnClick);
  }

  public async void SetImage(Profile p)
  {
    profile = p;
    Texture2D tex = await Singleton<GetImageService>.Instance.GetImage(p);
    image.sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
    OnImageLoaded.OnNext(profile);
  }

  public void OnClick()
  {
    OnClicked.OnNext(profile);
  }

  public void FadeIn(float time)
  {
    image.color = new Color(1, 1, 1, 0);
    image.DOFade(1.0f, time);
    faded = false;
  }

  public void FadeOut(float time)
  {
    image.DOFade(0, time);
    faded = true;
  }
}
