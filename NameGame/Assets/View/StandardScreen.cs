﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using WillowTree.NameGame.Core.Models;

public class StandardScreen : MonoBehaviour
{

  public Text nameField;
  public Transform Group;

  public Subject<bool> OnScreenResult = new Subject<bool>();

  private List<Profile> profiles = new List<Profile>();
  private List<ProfilePicture> pictures = new List<ProfilePicture>();
  private int readyCounter = 0;
  private CompositeDisposable subscriptions = new CompositeDisposable();

  private int hintsGiven = 0;
  private float timeForHint = 5f;
  private float currentTime = 0f;

  private int profilesPerRow = 2;
  private string correctName;

  public void SetName(string s)
  {
    nameField.text = s;
    correctName = s;
  }

  public void AddProfile(Profile p)
  {
    profiles.Add(p);
  }

  public bool ProfileExists(Profile p)
  {
    return profiles.Any(x => x.FullName == p.FullName);
  }

  public void LoadImages()
  {
    for (int i = 0; i < profiles.Count; i++)
    {
      Profile p = profiles[i];
      ProfilePicture picture = Instantiate(Resources.Load("ProfilePicture") as GameObject).GetComponent<ProfilePicture>();
      pictures.Add(picture);
      // picture.transform.SetParent(Group);
      picture.OnImageLoaded.Do(OnProfilePictureLoaded).Subscribe().AddTo(subscriptions);
      picture.OnClicked.Do(OnPictureClicked).Subscribe().AddTo(subscriptions);
      picture.SetImage(p);
    }
  }

  public void OnProfilePictureLoaded(Profile profile)
  {
    readyCounter++;

    if (readyCounter >= profiles.Count)
    {
      SetName(profiles[UnityEngine.Random.Range(0, profiles.Count)].FullName);
      ShowProfiles();
    }
  }

  public void ShowProfiles()
  {
    pictures.ForEach(picture =>
    {
      picture.transform.SetParent(Group);
      picture.FadeIn(.5f);
    });
  }

  public void FadeOut(Action callback)
  {
    StartCoroutine(FadeOutCoroutine(callback));
  }

  private IEnumerator FadeOutCoroutine(Action callback)
  {
    pictures.ForEach(picture => picture.FadeOut(.5f));
    yield return new WaitForSeconds(.5f);

    if (callback != null)
      callback();
  }

  public void OnPictureClicked(Profile p)
  {
    Singleton<StatsManager>.Instance.AddQuestion(new Question(profiles, correctName, p.FullName == correctName, hintsGiven));
    OnScreenResult.OnNext(p.FullName == correctName);
  }

  public void Update()
  {
    if (hintsGiven >= profiles.Count - 1)
      return;

    currentTime += Time.deltaTime;
    if (currentTime >= timeForHint)
    {
      currentTime -= timeForHint;
      hintsGiven++;
      List<ProfilePicture> ps = pictures.Where(x => x.profile.FullName != correctName && x.faded == false).ToList();
      ps[UnityEngine.Random.Range(0, ps.Count)].FadeOut(1.5f);
    }
  }


}
