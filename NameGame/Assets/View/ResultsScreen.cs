﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsScreen : MonoBehaviour
{

  public Text results;
	public Text title;
	public Button continueButton;
	private Text continueButtonText;

	private float fadeDuration = .5f;


	public void Awake()
	{
		continueButtonText = continueButton.GetComponentInChildren<Text>();
		continueButton.onClick.AddListener(StartFadeOut);
	}

	public void SetReults(int correct, int total)
	{
		results.text = string.Format("{0} / {1}", correct, total);
		FadeIn();
	}

	public void FadeIn()
	{
		results.color = new Color(0,0,0,0);
		title.color = new Color(0,0,0,0);
		continueButton.image.color = new Color(1,1,1,0);
		continueButtonText.color = new Color(0,0,0,0);

		results.DOFade(1, fadeDuration);
		title.DOFade(1, fadeDuration);
		continueButton.image.DOFade(1, fadeDuration);
		continueButtonText.DOFade(1, fadeDuration);
	}

	public void StartFadeOut()
	{
		StartCoroutine(FadeOut());
	}

	public IEnumerator FadeOut()
	{
		results.DOFade(0, fadeDuration);
		title.DOFade(0, fadeDuration);
		continueButton.image.DOFade(0, fadeDuration);
		continueButtonText.DOFade(0, fadeDuration);

		yield return new WaitForSeconds(fadeDuration);
		yield return null; //wait 1 extra frame

		Destroy(gameObject);
		MainMenuManager.Instance.Show();

	}
}
