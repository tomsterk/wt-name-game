using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using WillowTree.NameGame.Core.Models;
using WillowTree.NameGame.Core.Services;

public class GetImageService
{
  private Dictionary<string, Texture2D> cachedTextures = new Dictionary<string, Texture2D>();

  public GetImageService()
  {

  }

  public async Task<Texture2D> GetImage(Profile p)
  {
    Texture2D tex;
    if (cachedTextures.TryGetValue(p.FullName, out tex))
    {
      return tex;
    }

    string imageUrl = "http://" + p.Headshot.Url.Substring(2);
    byte[] bytes = await NameGameService.GetImageBytes(imageUrl);
    tex = BytesToTexture(bytes);
    if (cachedTextures.ContainsKey(p.FullName) == false)
      cachedTextures.Add(p.FullName, tex);
      
    return tex;
  }

  public Texture2D BytesToTexture(byte[] imageBytes)
  {
    Texture2D tex = new Texture2D(2, 2);
    tex.LoadImage(imageBytes);
    return tex;
  }
}