﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using WillowTree.NameGame.Core.Models;

namespace WillowTree.NameGame.Core.Services
{
  public class NameGameService
  {
    private static readonly string DataUrl = "https://www.willowtreeapps.com/api/v1.0/profiles";

    public static async Task<Profile[]> GetProfiles()
    {
      Profile[] profiles;
      //return 
      using (WebClient wc = new WebClient())
      {
        string json = await wc.DownloadStringTaskAsync(DataUrl);
        profiles = JsonConvert.DeserializeObject<Profile[]>(json);
      }


      // HttpClient client = new HttpClient();
      // HttpResponseMessage response = await client.GetAsync(DataUrl);
      // response.EnsureSuccessStatusCode();
      // string content = await response.Content.ReadAsStringAsync();

      foreach (Profile p in profiles)
      {
        //for debugging
      }
      return profiles;
    }

    public static async Task<byte[]> GetImageBytes(string url)
    {
      byte[] bytes;
      using (WebClient wc = new WebClient())
      {
        bytes = await wc.DownloadDataTaskAsync(url);
      }

      return bytes;
    }

    
  }
}
